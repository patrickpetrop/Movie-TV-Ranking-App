﻿using System;
using System.Collections.Generic;

namespace Rankker.Common.Domain.Model
{
    public class Movie : IModificationHistory
    {
        public Guid MovieId { get; set; }
        public string Name { get; set; }
        public string Tagline { get; set; }
        public string Overview { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public int RunTime { get; set; }
        public long Budget { get; set; }
        public long Revenue { get; set; }
        public long TmdbId { get; set; }
        public string ImdbId { get; set; }
        public string TmdbPosterPath { get; set; }
        public string TmdbBackdropPath { get; set; }
        public string Status { get; set; }
        public DateTime DateUpdated { get; set; }

        public List<MovieGenreRel> MovieGenreRels { get; set; } = new List<MovieGenreRel>();

        public MovieCollection MovieCollection { get; set; }

        public DateTime DateModified { get; set; }
        public DateTime DateCreated { get; set; }

    }
}
