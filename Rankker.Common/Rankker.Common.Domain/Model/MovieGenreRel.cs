﻿using System;

namespace Rankker.Common.Domain.Model
{
    public class MovieGenreRel : IModificationHistory
    {
        public Guid MovieId { get; set; }
        public Movie Movie { get; set; }

        public int MovieGenreId { get; set; }
        public MovieGenre MovieGenre { get; set; }

        public DateTime DateModified { get; set; }
        public DateTime DateCreated { get; set; }
    }
}