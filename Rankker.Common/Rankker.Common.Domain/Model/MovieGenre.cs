﻿using System;
using System.Collections.Generic;

namespace Rankker.Common.Domain.Model
{
    public class MovieGenre : IModificationHistory
    {
        public int MovieGenreId { get; set; }
        public string Source { get; set; }
        public string SourceName { get; set; }
        public int SourceId { get; set; }

        public List<MovieGenreRel> MovieGenreRels { get; set; } = new List<MovieGenreRel>();

        public DateTime DateModified { get; set; }
        public DateTime DateCreated { get; set; }
    }
}