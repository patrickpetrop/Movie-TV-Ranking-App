﻿using System;

namespace Rankker.Common.Domain.Model
{
    public interface IModificationHistory
    {
        DateTime DateModified { get; set; }
        DateTime DateCreated { get; set; }
    }
}