﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Rankker.Common.Domain.Services.Login
{
    public interface ILoginService
    {
        //TODO will need to return VALID User
        Task<bool> Login(string username, string password);
    }
}
