﻿using Rankker.Common.Domain.Model;

namespace Rankker.Common.Domain.Services.Communication
{
    public class MovieGenreResponse : BaseResponse
    {
        public MovieGenre MovieGenre { get; set; }
        public MovieGenreResponse(bool success, string message, MovieGenre movieGenre) : base(success, message)
        {
            MovieGenre = movieGenre;
        }

        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="movieGenre">Saved movie genre.</param>
        /// <returns>Response.</returns>
        public MovieGenreResponse(MovieGenre movieGenre) : this(true, string.Empty, movieGenre)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public MovieGenreResponse(string message) : this(false, message, null)
        { }
    }
}