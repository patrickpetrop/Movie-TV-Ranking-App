﻿using NUnit.Framework;

namespace Rankker.Common.Elo.UnitTests
{
    public class EloWarTests
    {
        public EloObject LeftEloObject;
        public EloObject RightEloObject;

        [SetUp]
        public void Setup()
        {
            LeftEloObject = new EloObject(1, "MyLeftObject");
            RightEloObject = new EloObject(2, "MyRightObject");
        }

        [Test]
        public void EloBattle_PickLeftAsWinner_ExpectThatLeftIsReturned()
        {
            var winner = EloEnum.EloBattleWinner.Left;

            var eloBattle = EloBattle.Instanciate(LeftEloObject, RightEloObject);

            eloBattle.SetWinner(winner);

            var actualWinner = eloBattle.EloBattleWinner;

            Assert.AreEqual(winner, actualWinner);
        }

        [Test]
        public void EloBattle_PickRightAsWinner_ExpectThatRightIsReturned()
        {
            var winner = EloEnum.EloBattleWinner.Right;

            var eloBattle = EloBattle.Instanciate(LeftEloObject, RightEloObject);

            eloBattle.SetWinner(winner);

            var actualWinner = eloBattle.EloBattleWinner;

            Assert.AreEqual(winner, actualWinner);
        }
    }
}