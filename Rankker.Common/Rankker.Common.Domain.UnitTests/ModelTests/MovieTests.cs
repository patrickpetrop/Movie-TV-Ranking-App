﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Rankker.Common.Domain.Model;

namespace Rankker.Common.Domain.UnitTests.ModelTests
{
    public class MovieTests
    {
        [Test]
        public void MyFirstTest()
        {
            var movie = new Movie
            {
                Name = "Breaking Bad"
            };

            Assert.AreEqual("Breaking Bad", movie.Name);
        }

    }
}
