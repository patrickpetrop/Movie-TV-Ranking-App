﻿using static Rankker.Common.Elo.EloEnum;

namespace Rankker.Common.Elo
{
    public class EloBattle
    {
        //Might need to be UUID
        public int Id { get; set; }

        public EloObject LeftEloObject { get; set; }
        public EloObject RightEloObject { get; set; }

        public EloBattleWinner EloBattleWinner { get; set; }

        public static EloBattle Instanciate(EloObject leftEloObject, EloObject rightEloObject)
        {
            var eloBattle = new EloBattle();
            eloBattle.LeftEloObject = leftEloObject;
            eloBattle.RightEloObject = rightEloObject;

            return eloBattle;
        }

        public void SetWinner(EloBattleWinner eloBattleWinner)
        {
            EloBattleWinner = eloBattleWinner;
        }
    }
}