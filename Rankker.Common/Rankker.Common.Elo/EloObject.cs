﻿namespace Rankker.Common.Elo
{
    public class EloObject
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public EloObject(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}