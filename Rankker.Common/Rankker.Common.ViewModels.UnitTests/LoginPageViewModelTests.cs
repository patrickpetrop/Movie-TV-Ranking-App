﻿using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Rankker.Common.Domain.Services.Login;
using Rankker.Common.ViewModels;
namespace Tests
{
    [TestFixture]
    public class LoginPageViewModelTests
    {
        private string _username;
        private string _password;

        [SetUp]
        public void SetUp()
        {
            _username = "Petro";
            _password = "Robo123";
        }

        [Test]
        public async Task InvalidParameters_FailedLogin_ToastIsShown_Async()
        {
            var toastIsFlagged = false;
            var mockLoginService = new Mock<ILoginService>();
            
            mockLoginService.Setup(x => x.Login(It.Is<string>(c => c.Equals(_username)),
                    It.Is<string>(c => c.Equals(_password))))
                .Returns(Task.FromResult(false));

            var viewModel = new LoginPageViewModel(mockLoginService.Object);

            viewModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName.Equals(nameof(LoginPageViewModel.Username)))
                {
                    Assert.AreEqual(_username, viewModel.Username);
                }
                if (e.PropertyName.Equals(nameof(LoginPageViewModel.Password)))
                {
                    Assert.AreEqual(_password, viewModel.Password);
                }
                if (e.PropertyName.Equals(nameof(LoginPageViewModel.FlagSomeToast)))
                {
                    toastIsFlagged = true;
                    Assert.IsTrue(viewModel.FlagSomeToast);
                }
            };
            //Set username and password, as if user typed it in
            viewModel.Username = _username;
            viewModel.Password = _password;

            //TODO unsure about this in tests
            await viewModel.LoginAsync();

            Assert.IsTrue(toastIsFlagged);
        }

        [Test]
        public async Task ValidParameters_SuccessfulLogin_LoginCompleted_Async()
        {
            var loginCompleted = false;
            var mockLoginService = new Mock<ILoginService>();


            mockLoginService.Setup(x => x.Login(It.Is<string>(c => c.Equals(_username)),
                                                    It.Is<string>(c => c.Equals(_password))))
                                                    .Returns(Task.FromResult(true));

            var viewModel = new LoginPageViewModel(mockLoginService.Object);

            viewModel.PropertyChanged += (s, e) =>
            {
                if (e.PropertyName.Equals(nameof(LoginPageViewModel.Username)))
                {
                    Assert.AreEqual(_username, viewModel.Username);
                }
                if (e.PropertyName.Equals(nameof(LoginPageViewModel.Password)))
                {
                    Assert.AreEqual(_password, viewModel.Password);
                }
                if (e.PropertyName.Equals(nameof(LoginPageViewModel.LoginCompleted)))
                {
                    loginCompleted = true;
                    Assert.IsTrue(viewModel.LoginCompleted);
                }
            };
            //Set username and password, as if user typed it in
            viewModel.Username = _username;
            viewModel.Password = _password;

            //TODO unsure about this in tests
            await viewModel.LoginAsync();

            Assert.IsTrue(loginCompleted);
        }
    }
}