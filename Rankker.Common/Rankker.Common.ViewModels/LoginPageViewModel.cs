﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Rankker.Common.Domain.Services.Login;

namespace Rankker.Common.ViewModels
{
    public class LoginPageViewModel : INotifyPropertyChanged
    {
        private bool _progressBarVisible;
        private bool _userNameTextFieldEnabled;
        private bool _passwordTextFieldEnabled;
        private bool _loginButtonEnabled;
        private string _username;
        private string _password;
        private bool _loginCompleted;
        //TODO need better way to flag a toast
        private bool _flagSomeToast;
        //TODO need a call to a service that would return if valid login attempt
        private ILoginService _loginService;

        public LoginPageViewModel(ILoginService loginService)
        {
            _progressBarVisible = false;
            _userNameTextFieldEnabled = true;
            _passwordTextFieldEnabled = true;
            _loginButtonEnabled = true;
            _flagSomeToast = false;
            _loginService = loginService;
        }

        public bool ProgressBarVisible
        {
            get { return _progressBarVisible; }
            set
            {
                if (_progressBarVisible != value)
                {
                    _progressBarVisible = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool LoginButtonEnabled
        {
            get { return _loginButtonEnabled; }
            set
            {
                if (_loginButtonEnabled != value)
                {
                    _loginButtonEnabled = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool UserNameTextFieldEnabled
        {
            get { return _userNameTextFieldEnabled; }
            set
            {
                if (_userNameTextFieldEnabled != value)
                {
                    _userNameTextFieldEnabled = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool PasswordTextFieldEnabled
        {
            get { return _passwordTextFieldEnabled; }
            set
            {
                if (_passwordTextFieldEnabled != value)
                {
                    _passwordTextFieldEnabled = value;
                    OnPropertyChanged();
                }
            }
        }

        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        public bool LoginCompleted
        {
            get { return _loginCompleted; }
            set
            {
                if (_loginCompleted != value)
                {
                    _loginCompleted = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool FlagSomeToast
        {
            get { return _flagSomeToast; }
            set
            {
                if (_flagSomeToast != value && value)
                {
                    _flagSomeToast = value;
                    OnPropertyChanged();
                }
                else
                {
                    _flagSomeToast = value;
                }
            }
        }

        public async Task LoginAsync()
        {
            ProgressBarVisible = true;
            LoginButtonEnabled = false;
            UserNameTextFieldEnabled = false;
            PasswordTextFieldEnabled = false;

            //TODO add check here for success
            if (await _loginService.Login(Username, Password))
            {
                LoginCompleted = true;
            }
            else
            {
                FlagSomeToast = true;
                ProgressBarVisible = false;
                LoginButtonEnabled = true;
                UserNameTextFieldEnabled = true;
                PasswordTextFieldEnabled = true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}