using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xunit;

namespace Rankker.Backend.API.IntegrationTests
{
    public class PopulateControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public PopulateControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task CanGetMovies()
        {
            // The endpoint or route of the controller action.
            var httpResponse = await _client.GetAsync("/api/populate");
            
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);

//            // Deserialize and examine results.
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
//            var players = JsonConvert.DeserializeObject<IEnumerable<Player>>(stringResponse);
//            Assert.Contains(players, p => p.FirstName == "Wayne");
//            Assert.Contains(players, p => p.FirstName == "Mario");
        }
    }
}
