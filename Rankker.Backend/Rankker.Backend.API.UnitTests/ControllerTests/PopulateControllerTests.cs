using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Rankker.Backend.API.Controllers;
using Rankker.Backend.API.Populating;
using Rankker.Backend.API.Repositories;
using Rankker.Backend.API.Repositories.Wrapper;
using Rankker.Backend.API.Resources;
using Rankker.Backend.API.Responses;
using Rankker.Common.Domain.Model;
using Rankker.Common.Domain.Services;

namespace Rankker.Backend.API.UnitTests.ControllerTests
{
    public class PopulateControllerTests
    {
        [SetUp]
        public void Setup()
        {
        }

//        [Test]
//        public void GetAll_Tests_Ensure_Service_Is_Called()
//        {
//            IEnumerable<Movie> movieList = new List<Movie>
//            {
//                new Movie()
//                {
//                    Name = "Interstellar"
//                },
//                new Movie()
//                {
//                    Name = "Pulp Fiction"
//                }
//            };
//
//            IEnumerable<MovieResource> movieResourceList = new List<MovieResource>()
//            {
//                new MovieResource()
//                {
//                    Name = "Help!"
//                },
//                new MovieResource()
//                {
//                    Name = "I'm Trapped"
//                }
//            };
//            var mockRepoWrapper = new Mock<IRepositoryWrapper>();
//            mockRepoWrapper.Setup(x => x.Movie.GetAllMoviesWithGenresAsync()).Returns(Task.FromResult(movieList));
//            
//            var mockMapper = new Mock<IMapper>();
//            mockMapper.Setup(m => m.Map<IEnumerable<Movie>, IEnumerable<MovieResource>>(It.IsAny<IEnumerable<Movie>>()))
//                .Returns(movieResourceList);
//
//            var populateController = new PopulateController(mockRepoWrapper.Object, mockMapper.Object, null);
//
//            var objectResult = populateController.GetAll().Result as ObjectResult;
//            if (objectResult == null)
//                Assert.Fail();
//            //TODO Need a Json response builder class and tests on that.
//            Assert.AreEqual((int)HttpStatusCode.OK, objectResult.StatusCode);
//
//            mockRepoWrapper.Verify(c => c.Movie.GetAllMoviesWithGenresAsync(), Times.Once);
//            mockMapper.Verify(c => c.Map<IEnumerable<Movie>, IEnumerable<MovieResource>>(It.IsAny<IEnumerable<Movie>>()), Times.Once);
//        }
//
//        [Test]
//        public void GetAll_Tests_NoData_Ensure_NotFoundResponse()
//        {
//            IEnumerable<Movie> emptyMovieList = new List<Movie>();
//            IEnumerable<MovieResource> emptyMovieResourceList = new List<MovieResource>();
//
//            var mockRepoWrapper = new Mock<IRepositoryWrapper>();
//            mockRepoWrapper.Setup(x => x.Movie.GetAllMoviesWithGenresAsync()).Returns(Task.FromResult(emptyMovieList));
//
//            var mockMapper = new Mock<IMapper>();
//            mockMapper.Setup(m => m.Map<IEnumerable<Movie>, IEnumerable<MovieResource>>(It.IsAny<IEnumerable<Movie>>()))
//                .Returns(emptyMovieResourceList);
//
//            var populateController = new PopulateController(mockRepoWrapper.Object, mockMapper.Object, null);
//
//            var objectResult = populateController.GetAll().Result as ObjectResult;
//            if (objectResult == null)
//                Assert.Fail();
//            //TODO Need a Json response builder class and tests on that.
//            Assert.AreEqual((int)HttpStatusCode.NotFound, objectResult.StatusCode);
//
//            mockRepoWrapper.Verify(c => c.Movie.GetAllMoviesWithGenresAsync(), Times.Once);
//            mockMapper.Verify(c => c.Map<IEnumerable<Movie>, IEnumerable<MovieResource>>(It.IsAny<IEnumerable<Movie>>()), Times.Never);
//        }

//        [Test]
//        public void PopulateMovieByTmdbId_MovieExists_ReturnConflict()
//        {
//            const int validTmdbId = 1212;
//
//            var validMovie = new Movie()
//            {
//                Name = "Casablanca",
//                TmdbId = validTmdbId
//            };
//
//            var validMovieResource = new MovieResource()
//            {
//            Name = "Casablanca",
//            TmdbId = validTmdbId
//            };
//
//            var mockRepoWrapper = new Mock<IRepositoryWrapper>();
//            mockRepoWrapper.Setup(x => x.Movie.FindByTmdbId(It.Is<int>(c => c == validTmdbId))).Returns(Task.FromResult(validMovie));
//
//            var mockMapper = new Mock<IMapper>();
//            mockMapper.Setup(m => m.Map<Movie, MovieResource>(It.Is<Movie>(c => c.Equals(validMovie))))
//                //TODO move to static objects, with full details
//                .Returns(validMovieResource);
//
//            var populateController = new PopulateController(mockRepoWrapper.Object, mockMapper.Object, null);
//
//            var objectResult = populateController.PopulateMovieByTmdbId(validTmdbId).Result as ObjectResult;
//            if (objectResult == null)
//                Assert.Fail();
//            //TODO Need a Json response builder class and tests on that.
//            var movieResourceResult = objectResult.Value as MovieResource;
//            Assert.AreEqual(movieResourceResult, validMovieResource);
//            Assert.AreEqual((int)HttpStatusCode.Conflict, objectResult.StatusCode);
//
//            mockRepoWrapper.Verify(c => c.Movie.FindByTmdbId(It.Is<int>(d => d == validTmdbId)), Times.Once);
//            mockMapper.Verify(c => c.Map<Movie, MovieResource>(It.Is<Movie>(d => d.Equals(validMovie))), Times.Once);
//        }
//
//        [Test]
//        public void PopulateMovieByTmdbId_MovieDoesNotExist_ReturnCreated()
//        {
//            const int newTmdbId = 1212;
//
//            var createdMovie = new Movie()
//            {
//                Name = "Casablanca",
//                TmdbId = newTmdbId
//            };
//
//            var createdMovieResource = new MovieResource()
//            {
//                Name = "Casablanca",
//                TmdbId = newTmdbId
//            };
//
//            var mockRepoWrapper = new Mock<IRepositoryWrapper>();
//            mockRepoWrapper.Setup(x => x.Movie.FindByTmdbId(It.Is<int>(c => c == newTmdbId))).Returns(Task.FromResult<Movie>(null));
//
//            var mockMapper = new Mock<IMapper>();
//            mockMapper.Setup(m => m.Map<Movie, MovieResource>(It.Is<Movie>(d => d.Equals(createdMovie))))
//                .Returns(createdMovieResource);
//
//            var mockTmdbMovieCreator = new Mock<ITmdbMovieCreator>();
//            mockTmdbMovieCreator.Setup(m => m.AddMovieToDatabase(It.Is<int>(c => c == newTmdbId), It.Is<IRepositoryWrapper>(d => d.Equals(mockRepoWrapper.Object))))
//                .Returns(Task.FromResult(createdMovie));
//
//            var populateController = new PopulateController(mockRepoWrapper.Object, mockMapper.Object, mockTmdbMovieCreator.Object);
//
//            var objectResult = populateController.PopulateMovieByTmdbId(newTmdbId).Result as ObjectResult;
//            if (objectResult == null)
//                Assert.Fail();
//            //TODO Need a Json response builder class and tests on that.
//            var movieResourceResult = objectResult.Value as MovieResource;
//            Assert.AreEqual(movieResourceResult, createdMovieResource);
//            Assert.AreEqual((int)HttpStatusCode.Created, objectResult.StatusCode);
//
//            mockRepoWrapper.Verify(c => c.Movie.FindByTmdbId(It.Is<int>(d => d == newTmdbId)), Times.Once);
//            mockMapper.Verify(c => c.Map<Movie, MovieResource>(It.Is<Movie>(d => d.Equals(createdMovie))), Times.Once);
//            mockTmdbMovieCreator.Verify(c => c.AddMovieToDatabase(It.Is<int>(d => d == newTmdbId), It.Is<IRepositoryWrapper>(d => d.Equals(mockRepoWrapper.Object))), Times.Once);
//        }
//
//        [Test]
//        public void PopulateMovieByTmdbId_InvalidTmdbid_ReturnNotFound()
//        {
//            const int invalidTmdbId = 12121;
//
//            var mockRepoWrapper = new Mock<IRepositoryWrapper>();
//            mockRepoWrapper.Setup(x => x.Movie.FindByTmdbId(It.Is<int>(c => c == invalidTmdbId))).Returns(Task.FromResult<Movie>(null));
//
//            var mockMapper = new Mock<IMapper>();
//            mockMapper.Setup(m => m.Map<Movie, MovieResource>(It.IsAny<Movie>()))
//                .Returns<MovieResource>(null);
//
//            var mockTmdbMovieCreator = new Mock<ITmdbMovieCreator>();
//            mockTmdbMovieCreator.Setup(m => m.AddMovieToDatabase(It.Is<int>(c => c == invalidTmdbId), It.Is<IRepositoryWrapper>(d => d.Equals(mockRepoWrapper.Object))))
//                .Returns(Task.FromResult<Movie>(null));
//
//            var populateController = new PopulateController(mockRepoWrapper.Object, mockMapper.Object, mockTmdbMovieCreator.Object);
//
//            var objectResult = populateController.PopulateMovieByTmdbId(invalidTmdbId).Result as ObjectResult;
//            if (objectResult == null)
//                Assert.Fail();
//            //TODO Need a Json response builder class and tests on that.
//            //TODO will have string response
//            Assert.AreEqual((int)HttpStatusCode.NotFound, objectResult.StatusCode);
//
//            mockRepoWrapper.Verify(c => c.Movie.FindByTmdbId(It.Is<int>(d => d == invalidTmdbId)), Times.Once);
//            mockMapper.Verify(c => c.Map<Movie, MovieResource>(It.IsAny<Movie>()), Times.Never());
//            mockTmdbMovieCreator.Verify(c => c.AddMovieToDatabase(It.Is<int>(d => d == invalidTmdbId), It.Is<IRepositoryWrapper>(d => d.Equals(mockRepoWrapper.Object))), Times.Once);
//        }
    }
}