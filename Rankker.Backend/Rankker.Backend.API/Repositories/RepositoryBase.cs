﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.Internal;

namespace Rankker.Backend.API.Repositories
{
    //https://stackoverflow.com/questions/46374252/how-to-write-repository-method-for-theninclude-in-ef-core-2


    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected RankkerContext Context { get; set; }

        protected RepositoryBase(RankkerContext rankkerContext)
        {
            this.Context = rankkerContext;
        }

        public async Task<IEnumerable<T>> FindAllAsync(Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            IQueryable<T> query = Context.Set<T>();

            if (include != null)
            {
                query = include(query);
            }
            return await query.ToListAsync();
        }

        public async Task<IEnumerable<T>> FindByConditionAsync(Expression<Func<T, bool>> expression,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            IQueryable<T> query = Context.Set<T>();

            if (include != null)
            {
                query = include(query);
            }

            return await query.Where(expression).ToListAsync();
        }

        public async Task<T> FindSingleByConditionAsync(Expression<Func<T, bool>> expression,
                    Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            IQueryable<T> query = Context.Set<T>();

            if (include != null)
            {
                query = include(query);
            }

            return await query.Where(expression).FirstOrDefaultAsync();
        }

        public void Create(T entity)
        {
            this.Context.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            this.Context.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            this.Context.Set<T>().Remove(entity);
        }

        public async Task SaveAsync()
        {//TODO Pass in Cancellation Token
            await this.Context.SaveChangesAsync();
        }
    }
}