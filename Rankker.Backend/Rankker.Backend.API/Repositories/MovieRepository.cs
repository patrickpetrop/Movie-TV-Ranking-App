﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Rankker.Common.Domain.Model;

namespace Rankker.Backend.API.Repositories
{
    public class MovieRepository : RepositoryBase<Movie>, IMovieRepository
    {
        public MovieRepository(RankkerContext rankkerContext) : base(rankkerContext)
        {
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            var movies = await FindAllAsync();
            return movies.OrderBy(x => x.Name);
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesWithGenresAsync()
        {
            //TODO need better way to do this
            var movies = await Context.Movies
                .Include(x => x.MovieGenreRels)
                .ThenInclude(y => y.MovieGenre)
                .ToListAsync();
            return movies.OrderBy(x => x.Name);
        }

        public async Task<Movie> FindByTmdbId(int tmdbId)
        {
            return await FindSingleByConditionAsync(x => x.TmdbId == tmdbId, 
                include: source => source
                .Include(x => x.MovieGenreRels)
                .ThenInclude(y => y.MovieGenre));
        }
    }
}