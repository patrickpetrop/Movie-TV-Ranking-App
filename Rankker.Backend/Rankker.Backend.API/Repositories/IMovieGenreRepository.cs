﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Rankker.Common.Domain.Model;

namespace Rankker.Backend.API.Repositories
{
    public interface IMovieGenreRepository : IRepositoryBase<MovieGenre>
    {
        Task<IEnumerable<MovieGenre>> GetAllMovieGenresAsync();
        Task CreateMovieGenreAsync(MovieGenre movieGenre);
    }
}