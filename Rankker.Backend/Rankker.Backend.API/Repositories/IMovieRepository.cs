﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Rankker.Common.Domain.Model;

namespace Rankker.Backend.API.Repositories
{
    public interface IMovieRepository : IRepositoryBase<Movie>
    {
        Task<IEnumerable<Movie>> GetAllMoviesAsync();
        Task<IEnumerable<Movie>> GetAllMoviesWithGenresAsync();
        Task<Movie> FindByTmdbId(int tmdbId);


    }
}