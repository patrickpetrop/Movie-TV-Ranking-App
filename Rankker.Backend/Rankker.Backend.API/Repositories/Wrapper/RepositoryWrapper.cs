﻿using System.Threading.Tasks;

namespace Rankker.Backend.API.Repositories.Wrapper
{
    public class RepositoryWrapper: IRepositoryWrapper
    {
        private readonly RankkerContext _context;
        private IMovieRepository _movie;
        public IMovieRepository Movie => _movie ?? (_movie = new MovieRepository(_context));

        private IMovieGenreRepository _movieGenre;
        public IMovieGenreRepository MovieGenre => _movieGenre ?? (_movieGenre = new MovieGenreRepository(_context));
        private IMovieCollectionRepository _movieCollection;
        public IMovieCollectionRepository MovieCollection => _movieCollection ?? (_movieCollection = new MovieCollectionRepository(_context));

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public RepositoryWrapper(RankkerContext rankkerContext)
        {
            _context = rankkerContext;
        }
    }
}