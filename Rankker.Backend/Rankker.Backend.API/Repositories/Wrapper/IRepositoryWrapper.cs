﻿using System.Threading.Tasks;

namespace Rankker.Backend.API.Repositories.Wrapper
{
    public interface IRepositoryWrapper
    {
        IMovieRepository Movie { get; }
        IMovieGenreRepository MovieGenre { get; }
        IMovieCollectionRepository MovieCollection { get; }
        void Save();
        Task SaveAsync();

    }
}