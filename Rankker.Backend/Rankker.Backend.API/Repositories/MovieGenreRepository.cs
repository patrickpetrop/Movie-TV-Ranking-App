﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Rankker.Common.Domain.Model;

namespace Rankker.Backend.API.Repositories
{
    public class MovieGenreRepository : RepositoryBase<MovieGenre>, IMovieGenreRepository
    {
        public MovieGenreRepository(RankkerContext rankkerContext) : base(rankkerContext)
        {
        }

        public async Task<IEnumerable<MovieGenre>> GetAllMovieGenresAsync()
        {
            var movieGenres = await FindAllAsync();
            return movieGenres.OrderBy(x => x.SourceName);
        }

        public async Task CreateMovieGenreAsync(MovieGenre movieGenre)
        {
            Create(movieGenre);
            await SaveAsync();
        }
    }
}