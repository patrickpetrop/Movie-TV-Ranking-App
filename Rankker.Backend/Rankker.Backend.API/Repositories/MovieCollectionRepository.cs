﻿using Rankker.Common.Domain.Model;

namespace Rankker.Backend.API.Repositories
{
    public class MovieCollectionRepository: RepositoryBase<MovieCollection>, IMovieCollectionRepository
    {
        public MovieCollectionRepository(RankkerContext rankkerContext) : base(rankkerContext)
        {
        }
    }
}