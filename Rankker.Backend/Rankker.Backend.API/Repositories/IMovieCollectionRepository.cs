﻿using Rankker.Common.Domain.Model;

namespace Rankker.Backend.API.Repositories
{
    public interface IMovieCollectionRepository : IRepositoryBase<MovieCollection>
    {
        
    }
}