﻿using System.Net;
using Rankker.Backend.API.Resources;
using Rankker.Common.Domain.Model;

namespace Rankker.Backend.API.Responses
{
    public class MovieResponse : BaseResponse<Movie>
    {
        public MovieResource MovieResource { get; set;}


        public MovieResponse(Movie resource, HttpStatusCode statusCode) : base(resource, "", statusCode)
        {
        }
    }
}