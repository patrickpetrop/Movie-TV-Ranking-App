﻿using System.Net;
using Microsoft.AspNetCore.Http;

namespace Rankker.Backend.API.Responses
{
    public abstract class BaseResponse<T> where T : class
    {
        private HttpStatusCode StatusCode { get; }
        public T Resource { get; set; }
        public string Message{ get; set; }

        protected BaseResponse(T resource, string message, HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
            Message = message;
            Resource = resource;
        }

        public int GetStatusCode()
        {
            return (int) StatusCode;
        }
    }
}