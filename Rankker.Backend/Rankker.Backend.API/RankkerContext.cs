﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Rankker.Common.Domain.Model;

namespace Rankker.Backend.API
{
    public class RankkerContext : DbContext
    {
        public RankkerContext(DbContextOptions<RankkerContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>()
                .HasKey(c => c.MovieId);

            modelBuilder.Entity<MovieGenre>()
                .HasKey(c => c.MovieGenreId);

            modelBuilder.Entity<MovieGenreRel>()
                .HasKey(k => new { k.MovieId, k.MovieGenreId });

            modelBuilder.Entity<MovieGenreRel>()
                .HasOne(x => x.Movie)
                .WithMany(x => x.MovieGenreRels)
                .HasForeignKey(x => x.MovieId);

            modelBuilder.Entity<MovieGenreRel>()
                .HasOne(x => x.MovieGenre)
                .WithMany(x => x.MovieGenreRels)
                .HasForeignKey(x => x.MovieGenreId);

            modelBuilder.Entity<MovieCollection>()
                .HasKey(c => c.MovieCollectionId);

            modelBuilder.Entity<MovieCollection>()
                .HasMany(c => c.Movies)
                .WithOne(e => e.MovieCollection)
                .IsRequired(false);

        }

        public override int SaveChanges()
        {
            //Allows DateCreated and DateModified to be set each time a save is performed
            foreach (var history in ChangeTracker.Entries()
                .Where(e => e.Entity is IModificationHistory &&
                            (e.State == EntityState.Added || e.State == EntityState.Modified))
                .Select(e => e.Entity as IModificationHistory))
            {
                history.DateModified = DateTime.Now;
                if (history.DateCreated == DateTime.MinValue)
                {
                    history.DateCreated = DateTime.Now;
                }
            }


            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            //Allows DateCreated and DateModified to be set each time a save is performed
            foreach (var history in ChangeTracker.Entries()
                .Where(e => e.Entity is IModificationHistory &&
                            (e.State == EntityState.Added || e.State == EntityState.Modified))
                .Select(e => e.Entity as IModificationHistory))
            {
                history.DateModified = DateTime.UtcNow;
                if (history.DateCreated == DateTime.MinValue)
                {
                    history.DateCreated = DateTime.UtcNow;
                }
            }
            return base.SaveChangesAsync(cancellationToken);
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieGenre> MovieGenres { get; set; }
        public DbSet<MovieGenreRel> MovieGenreRels { get; set; }
        public DbSet<MovieCollection> MovieCollections { get; set; }
    }
}