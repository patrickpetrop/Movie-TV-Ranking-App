﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Rankker.Common.Domain.Model;

namespace Rankker.Backend.API.Resources
{
    public class MovieResource
    {
        public string Name { get; set; }
        public string Overview { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public int RunTime { get; set; }
        public long TmdbId { get; set; }
        public string ImdbId { get; set; }
        public string TmdbPosterPath { get; set; }
        public string TmdbBackdropPath { get; set; }

        public IList<MovieGenreResource> MovieGenres { get; set; }

        public static MovieResource Instantiate(IMapper mapper, Movie movie)
        {
            return mapper.Map<Movie, MovieResource>(movie);
        }

        public static IEnumerable<MovieResource> InstantiateList(IMapper mapper, IEnumerable<Movie> movieList)
        {
            return mapper.Map<IEnumerable<Movie>, IEnumerable<MovieResource>>(movieList);
        }

    }
}