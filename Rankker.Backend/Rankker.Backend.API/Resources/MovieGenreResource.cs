﻿namespace Rankker.Backend.API.Resources
{
    public class MovieGenreResource
    {
        public string Source { get; set; }
        public string SourceName { get; set; }
        public int SourceId { get; set; }
    }
}