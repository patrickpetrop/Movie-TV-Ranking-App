﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NLog;
using Rankker.Backend.API.Logging;
using Rankker.Backend.API.Mapping;
using Rankker.Backend.API.Populating;
using Rankker.Backend.API.Repositories;
using Rankker.Backend.API.Repositories.Wrapper;
using Rankker.Common.Domain.Services;
using VMD.RESTApiResponseWrapper.Core.Extensions;

namespace Rankker.Backend.API
{
    public class Startup
    {
        public Startup(IHostingEnvironment end, IConfiguration configuration)
        {
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSingleton(Configuration);
            services.AddDbContext<RankkerContext>
                (options => options.UseSqlServer(Configuration.GetConnectionString("RankkerDatabase")));
            
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
            services.AddScoped<ITmdbMovieCreator, TmdbMovieCreator>();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ModelToResourceProfile());
                // need mapper file to go from resource to model
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddSingleton<ILoggerManager, NLogLoggerManager>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            //app.UseAPIResponseWrapperMiddleware();
            app.UseMvc();
        }
    }
}

