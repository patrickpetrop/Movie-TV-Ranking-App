﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using Rankker.Backend.API.Populating;
using Rankker.Backend.API.Repositories.Wrapper;
using Rankker.Backend.API.Resources;
using Rankker.Common.Domain.Model;
using Rankker.Common.Domain.Services;
using Rankker.Common.Tmdb.Imdb;
using Rankker.Common.Tmdb.Services;

namespace Rankker.Backend.API.Controllers
{
   [Route("api/[controller]")]
   [Produces("application/json")]
    public class PopulateController : Controller
   {
       private readonly IRepositoryWrapper _repositoryWrapper;
       private readonly IMapper _mapper;
       private readonly ITmdbMovieCreator _tmdbMovieCreator;
       private readonly string _tmdbApiKey;
       public PopulateController(IRepositoryWrapper repositoryWrapper, IMapper mapper, 
           ITmdbMovieCreator tmdbMovieCreator, IConfiguration configuration)
       {
           _repositoryWrapper = repositoryWrapper;
           _mapper = mapper;
           _tmdbMovieCreator = tmdbMovieCreator;
           _tmdbApiKey = configuration.GetValue<string>("ApiKeys:themoviedb");
       }

        [HttpGet("moviegenre")]
       public async Task<ActionResult> PopulateTmdbGenres()
       {
           var result = await _tmdbMovieCreator.AddTmdbGenresToDatabase(_repositoryWrapper, _tmdbApiKey);

           return new ContentResult
           {
               Content = new JObject() { { "message", result ? "Successful" : "Fail" } }.ToString(),
               ContentType = "application/json",
               StatusCode = result ? (int)200 : (int)404
           };
       }

       [HttpGet]
        public async Task<ActionResult> GetAll()
       {
           var result = await _repositoryWrapper.Movie.GetAllMoviesWithGenresAsync();
           if (!result.Any())
           {
               //TODO Need resource file to pass in string
               return new ObjectResult("No movies found") { StatusCode = (int)HttpStatusCode.NotFound };
           }

           return new ObjectResult(MovieResource.InstantiateList(_mapper, result)) { StatusCode = (int)HttpStatusCode.OK };
       }

        [HttpPut("movie/discover/{pagenumber}")]
        public async Task<ActionResult> PopulateMovieByDiscoverPage(int pageNumber)
        {
            for (int i = 1; i <= pageNumber; i++)
            {
                var result =
                    await _tmdbMovieCreator.PopulateMoviesByDiscoverPage(i, _repositoryWrapper,_tmdbApiKey);
            }

            return new ContentResult
            {
                Content = new JObject() { { "message", true ? "Successful" : "Fail" } }.ToString(),
                ContentType = "application/json",
                StatusCode = true ? (int)200 : (int)404
            };
        }

        [HttpPut("movie/{id}")]
       public async Task<ActionResult> PopulateMovieByTmdbId(int id)
       {
           //TODO add force update in api call?????
//           var foundMovie = await _repositoryWrapper.Movie.FindByTmdbId(id);
//           if (foundMovie != null)
//           {
//               //TODO can do date check, maybe call Update and send appropriate response
//               return new ObjectResult(MovieResource.Instantiate(_mapper, foundMovie))
//               {
//                   StatusCode = (int)HttpStatusCode.Conflict
//               };
//           }
           
           var movie = await _tmdbMovieCreator.AddMovieToDatabase(id,
               _repositoryWrapper, _tmdbApiKey);

           //TODO better way to manage null
           return movie == null ? 
               //TODO resource file
               new ObjectResult("Error when trying to add movie") {StatusCode = (int)HttpStatusCode.NotFound} :
               new ObjectResult(MovieResource.Instantiate(_mapper, movie)) { StatusCode = (int)HttpStatusCode.Created };
       }


       [HttpPost("imdb/{imdbid}")]
       public async Task<ActionResult> PopulateListFromImdb(string imdbid)
       {

            var imdbMovieList = new ImdbMovieList(imdbid);

            await imdbMovieList.GetListOfMovies();

            return Json(imdbMovieList.ListDescription +"  " + imdbMovieList.ListSize);

        }

//       [HttpDelete("movie/{id}")]
//       public async Task<ActionResult> PopulateMovieByTmdbId(int id)
//       {
//           var movieResponse = await TmdbMovieCreator.AddMovieToDatabase(id,
//               _repositoryWrapper);
//           if (!movieResponse.Success)
//           {
//
//           }
//           return new ContentResult
//           {
//               Content = new JObject() { { "message", result.Success ? "Successful" : "Fail" } }.ToString(),
//               ContentType = "application/json",
//               StatusCode = result.Success ? (int)200 : (int)404
//           };
//       }
    }
 }
