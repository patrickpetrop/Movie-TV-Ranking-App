﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Rankker.Backend.API.Resources;
using Rankker.Common.Domain.Model;

namespace Rankker.Backend.API.Mapping
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Movie, MovieResource>()
                .ForMember(dto => dto.MovieGenres,
                    opt => opt.MapFrom(x => x.MovieGenreRels.Select(y => y.MovieGenre).ToList()));


            CreateMap<MovieGenre, MovieGenreResource>();
        }
    }
}
