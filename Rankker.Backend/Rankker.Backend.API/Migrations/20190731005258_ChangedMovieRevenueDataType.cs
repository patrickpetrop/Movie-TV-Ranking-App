﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Rankker.Backend.API.Migrations
{
    public partial class ChangedMovieRevenueDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "Revenue",
                table: "Movies",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<long>(
                name: "Budget",
                table: "Movies",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Revenue",
                table: "Movies",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<int>(
                name: "Budget",
                table: "Movies",
                nullable: false,
                oldClrType: typeof(long));
        }
    }
}
