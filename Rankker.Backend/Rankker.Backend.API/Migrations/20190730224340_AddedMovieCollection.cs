﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Rankker.Backend.API.Migrations
{
    public partial class AddedMovieCollection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "MovieCollectionId",
                table: "Movies",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MovieCollections",
                columns: table => new
                {
                    MovieCollectionId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Overview = table.Column<string>(nullable: true),
                    TmdbId = table.Column<long>(nullable: false),
                    TmdbPosterPath = table.Column<string>(nullable: true),
                    TmdbBackdropPath = table.Column<string>(nullable: true),
                    DateModified = table.Column<DateTime>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCollections", x => x.MovieCollectionId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movies_MovieCollectionId",
                table: "Movies",
                column: "MovieCollectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Movies_MovieCollections_MovieCollectionId",
                table: "Movies",
                column: "MovieCollectionId",
                principalTable: "MovieCollections",
                principalColumn: "MovieCollectionId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Movies_MovieCollections_MovieCollectionId",
                table: "Movies");

            migrationBuilder.DropTable(
                name: "MovieCollections");

            migrationBuilder.DropIndex(
                name: "IX_Movies_MovieCollectionId",
                table: "Movies");

            migrationBuilder.DropColumn(
                name: "MovieCollectionId",
                table: "Movies");
        }
    }
}
