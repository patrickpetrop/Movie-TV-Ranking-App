﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using Rankker.Backend.API.Repositories.Wrapper;
using Rankker.Common.Domain.Model;
using Rankker.Common.Domain.Services;
using Rankker.Common.Domain.Services.Communication;
using Rankker.Common.Tmdb;
using Rankker.Common.Tmdb.Parsers;
using Rankker.Common.Tmdb.Services;

namespace Rankker.Backend.API.Populating
{
    public class TmdbMovieCreator : ITmdbMovieCreator
    {
        
        public async Task<bool> AddTmdbGenresToDatabase(IRepositoryWrapper repositoryWrapper, string apiKey)
        {
            var currentGenres = await repositoryWrapper.MovieGenre.GetAllMovieGenresAsync();
            if (currentGenres.Any())
            {
                return false;
            }

            var genres = await TmdbMovieService.GetListOfMovieGenres(apiKey);
            foreach (var movieGenre in genres)
            {
                await repositoryWrapper.MovieGenre.CreateMovieGenreAsync(movieGenre);
            }
            return true;
        }

        public async Task<bool> PopulateMoviesByDiscoverPage(int pageNumber,
            IRepositoryWrapper repository, string apiKey)
        {
            try { 
                var listOfIds = await TmdbMovieService.GetListOfMovieIdsForDiscoverPage(apiKey, pageNumber);


                foreach (var id in listOfIds)
                {
                    await AddMovieToDatabase(id, repository, apiKey);
                    await Task.Delay(400);
                }
                return true;
            }
            catch(Exception e)
            {
                 //TODO add logging
                 
                 return false;
            }
        }


        public async Task<MovieCollection> AddMovieCollectionToDatabase(int collectionId,
            IRepositoryWrapper repository, string apiKey)
        {
            try
            {
                var parser = await TmdbMovieService.GetMovieCollectionByTmdbId(collectionId, apiKey);

                var foundCollection =
                    await repository.MovieCollection.FindSingleByConditionAsync(c => c.TmdbId == collectionId);
                
                var movieCollection = parser.CreateOrUpdateMovieCollection(foundCollection);

                if(movieCollection.MovieCollectionId == Guid.Empty)
                {
                    repository.MovieCollection.Create(movieCollection);
                }
                else
                {
                    repository.MovieCollection.Update(movieCollection);
                }

                await repository.SaveAsync();

                foreach (var collectionMovieId in parser.GetCollectionMovieIds())
                {
                    var movie = await AddSingleMovieToDatabase(collectionMovieId, repository, apiKey);


                    movieCollection.Movies.Add(movie);
                    repository.MovieCollection.Update(movieCollection);
                    await repository.SaveAsync();
                    await Task.Delay(400);
                }

                return movieCollection;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public async Task<Movie> AddMovieToDatabase(int movieId,
            IRepositoryWrapper repository, string apiKey)
        {
            try
            {
                var parser = await TmdbMovieService.GetMovieByTmdbId(movieId, apiKey);

                if (parser.CollectionInt() != 0)
                {
                    var movieCollection = await AddMovieCollectionToDatabase(parser.CollectionInt(), repository,
                        apiKey);

                    return movieCollection.Movies.FirstOrDefault(c => c.TmdbId == movieId);
                }

                return await AddSingleMovieToDatabase(movieId, repository, apiKey);
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public async Task<Movie> AddSingleMovieToDatabase(int movieId,
            IRepositoryWrapper repository, string apiKey)
        {

            try
            {
                var parser = await TmdbMovieService.GetMovieByTmdbId(movieId, apiKey);

                var foundMovie = await repository.Movie.FindByTmdbId(movieId);

                var movie = parser.CreateOrUpdateMovie(foundMovie);

                var genreArray = parser.GenreArray();

                //Checked to ensure is TMDB when list is loaded from memory
                var movieGenreList = (await repository.MovieGenre.GetAllMovieGenresAsync()).ToList();

                foreach (var item in genreArray)
                {
                    var genre = movieGenreList.First(x => x.SourceId == (int)item["id"]);
                    if(movie.MovieGenreRels.Count(c => c.MovieGenreId == genre.MovieGenreId) == 0)
                        movie.MovieGenreRels.Add(new MovieGenreRel()
                        {
                            Movie = movie,
                            MovieGenre = genre
                        });
                }

                if (movie.MovieId == Guid.Empty)
                {
                    repository.Movie.Create(movie);
                }
                else
                {
                    repository.Movie.Update(movie);
                }
                
                await repository.SaveAsync();
                return movie;
            }
            catch (Exception e)
            {
                //TODO add logging
                Console.WriteLine("My error is " + e);
                return null;
            }
        }
    }
}
