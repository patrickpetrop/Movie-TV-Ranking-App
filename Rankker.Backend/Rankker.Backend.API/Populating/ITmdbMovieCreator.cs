﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Rankker.Backend.API.Repositories.Wrapper;
using Rankker.Common.Domain.Model;

namespace Rankker.Backend.API.Populating
{
    public interface ITmdbMovieCreator
    {
        Task<bool> AddTmdbGenresToDatabase(IRepositoryWrapper repositoryWrapper, string apiKey);

        Task<Movie> AddSingleMovieToDatabase(int movieId, IRepositoryWrapper repositoryWrapper,
            string apiKey);

        Task<bool> PopulateMoviesByDiscoverPage(int pageNumber, IRepositoryWrapper repository,
            string apiKey);

        Task<Movie> AddMovieToDatabase(int movieId,
            IRepositoryWrapper repository, string apiKey);

        Task<MovieCollection> AddMovieCollectionToDatabase(int collectionId,
            IRepositoryWrapper repository, string apiKey);
    }
}